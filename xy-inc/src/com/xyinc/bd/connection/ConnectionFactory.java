package com.xyinc.bd.connection;

import java.sql.Connection;

public class ConnectionFactory {

	private Connection connection;

	public Connection openConnection() {
		
		String uri = "jdbc:mysql://localhost:3306/xy-inc";
		String user = "root";
		String password = "root";

		try {

			Class.forName("org.gjt.mm.mysql.Driver");

			connection = java.sql.DriverManager.getConnection(uri, user, password);
		} catch (Exception e) {

			e.printStackTrace();

		}

		return connection;
	}

	public void closeConnection() {

		try {
			connection.close();
		} catch (Exception e) {

			e.printStackTrace();

		}
	}
}
