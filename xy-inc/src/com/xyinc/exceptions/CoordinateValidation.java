package com.xyinc.exceptions;

import com.xyinc.objects.Coordinate;

public class CoordinateValidation extends XyException {

	private static final long serialVersionUID = 1L;
	
	private String msg = "";

	public void checkCoordinate(Coordinate coordinate) throws XyException {

		if (coordinate.getName() == null || coordinate.getName().trim().equals("")) {
			msg += "nome � obrigat�rio ";
		}
		
		if (coordinate.getCoordinateX() < 0) {
			msg += "Coordenada X n�o pode ser negativo ";
		}
		
		if (coordinate.getCoordinateY() < 0) {
			msg += "Coordenada Y n�o pode ser negativo ";
		}
		
		if(!msg.isEmpty()){
			throw new XyException(msg);
		}

	}

}
