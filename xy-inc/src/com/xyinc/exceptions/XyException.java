package com.xyinc.exceptions;

public class XyException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public XyException(String message, Throwable cause){
		super(message, cause);
	}
	
	public XyException(){
		super();
	}
	
	public XyException(String message) {
        super(message);               
    }
 
    public XyException(Throwable cause) {
        super(cause);        
    }

}
