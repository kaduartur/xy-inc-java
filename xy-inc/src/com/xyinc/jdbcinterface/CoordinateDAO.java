package com.xyinc.jdbcinterface;

import java.util.List;

import com.xyinc.exceptions.XyException;
import com.xyinc.objects.Coordinate;

public interface CoordinateDAO {

	public Coordinate createCoodinate(Coordinate coordinate) throws XyException;

	public List<Coordinate> getAllCoordinates() throws XyException;

}
