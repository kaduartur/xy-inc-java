package com.xyinc.rest.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xyinc.bd.connection.ConnectionFactory;
import com.xyinc.enums.ExceptionMsg;
import com.xyinc.exceptions.CoordinateValidation;
import com.xyinc.exceptions.XyException;
import com.xyinc.jdbc.JDBCCoordinateDAO;
import com.xyinc.jdbcinterface.CoordinateDAO;
import com.xyinc.objects.Coordinate;

public class CoordinateService {
	
	private ConnectionFactory connectionFactory;

	public Coordinate createCoordinate(String params) throws Exception {
		
		try{
			Coordinate coordinate = new ObjectMapper().readValue(params, Coordinate.class);
			
			CoordinateValidation validation = new CoordinateValidation();
			validation.checkCoordinate(coordinate);			

			connectionFactory = new ConnectionFactory();
			Connection connection = connectionFactory.openConnection();
			
			CoordinateDAO jdbcCoordinate = new JDBCCoordinateDAO(connection);
			return jdbcCoordinate.createCoodinate(coordinate);			
			
		}catch (Exception e) {
			e.printStackTrace();
			throw new XyException(ExceptionMsg.POST.getValue(), e);
		}finally {
			if(connectionFactory != null){
				connectionFactory.closeConnection();
			}
		}		
	}

	public List<Coordinate> getAllCoordinates() throws Exception{

		try{

			connectionFactory = new ConnectionFactory();
			Connection connection = connectionFactory.openConnection();
			
			CoordinateDAO jdbcCoordinate = new JDBCCoordinateDAO(connection);
			return jdbcCoordinate.getAllCoordinates();			
			
		}catch (Exception e) {
			e.printStackTrace();
			throw new XyException(ExceptionMsg.GET.getValue(), e);
		}finally {
			if(connectionFactory != null){
				connectionFactory.closeConnection();
			}
		}
	}

	public List<Coordinate> getPointsInterestByProximity(int x, int y) throws Exception {
		
		int maxDistance = x - y;
		List<Coordinate> coordinatesFiltered = new ArrayList<Coordinate>();
		
		try{

			connectionFactory = new ConnectionFactory();
			Connection connection = connectionFactory.openConnection();
			
			CoordinateDAO jdbcCoordinate = new JDBCCoordinateDAO(connection);
			List<Coordinate> coordinates = jdbcCoordinate.getAllCoordinates();		
			
			for(Coordinate coordinate : coordinates){
				
				int a = Math.abs(coordinate.getCoordinateX() - x);
				int b = Math.abs(coordinate.getCoordinateY() - y);
				
				int total = a + b;
				
				if(total <= maxDistance){
					coordinatesFiltered.add(coordinate);
				}
				
			}
			
			return coordinatesFiltered;
			
		}catch (Exception e) {
			e.printStackTrace();
			throw new XyException(ExceptionMsg.GET.getValue(), e);
		}finally {
			if(connectionFactory != null){
				connectionFactory.closeConnection();
			}
		}
	}
	
	

}
