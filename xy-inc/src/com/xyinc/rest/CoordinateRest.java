package com.xyinc.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.xyinc.rest.service.CoordinateService;
import com.xyinc.util.UtilRest;

@Path("coordinateRest")
public class CoordinateRest extends UtilRest {

	public CoordinateRest() {

	}

	@POST
	@Path("/createCoordinate")
	@Consumes("application/*")
	public Response createCoordinate(String params) {

		try {

			CoordinateService coordinateService = new CoordinateService();

			return this.buildResponse(coordinateService.createCoordinate(params));
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}

	}

	@GET
	@Path("/getAllCoordinates")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCoordinates() {

		try {

			CoordinateService coordinateService = new CoordinateService();

			return this.buildResponse(coordinateService.getAllCoordinates());
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}
	
	@GET
	@Path("/getPointsInterestByProximity/{coordinateX}/{coordinateY}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPointsInterestByProximity(@PathParam("coordinateX") int x, @PathParam("coordinateY") int y) {

		try {

			CoordinateService coordinateService = new CoordinateService();

			return this.buildResponse(coordinateService.getPointsInterestByProximity(x, y));
		} catch (Exception e) {
			e.printStackTrace();
			return this.buildErrorResponse(e.getMessage());
		}
	}

}
