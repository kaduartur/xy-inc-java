package com.xyinc.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.xyinc.enums.ExceptionMsg;
import com.xyinc.exceptions.XyException;
import com.xyinc.jdbcinterface.CoordinateDAO;
import com.xyinc.objects.Coordinate;

public class JDBCCoordinateDAO implements CoordinateDAO {

	private Connection connection;

	public JDBCCoordinateDAO(Connection connection) {
		this.connection = connection;
	}

	public Coordinate createCoodinate(Coordinate coordinate) throws XyException {

		String sql = "INSERT INTO coordinates " + "(name, coordinate_y, coordinate_x) " + "VALUES(?,?,?)";

		PreparedStatement p;

		try {

			p = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			p.setString(1, coordinate.getName());
			p.setInt(2, coordinate.getCoordinateY());
			p.setInt(3, coordinate.getCoordinateX());
			p.execute();

			ResultSet rs = p.getGeneratedKeys();
			if (rs.next()) {
				coordinate.setId(rs.getInt(1));
			}

			return coordinate;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new XyException(ExceptionMsg.SQL.getValue(), e);
		}

	}

	public List<Coordinate> getAllCoordinates() throws XyException {
		String sql = "SELECT * FROM coordinates";

		PreparedStatement p;
		ResultSet rs;

		try {

			List<Coordinate> coordinateList = new ArrayList<Coordinate>();
			Coordinate coordinate = null;

			p = connection.prepareStatement(sql);
			rs = p.executeQuery();

			while (rs.next()) {

				coordinate = new Coordinate();

				coordinate.setId(rs.getInt("id"));
				coordinate.setName(rs.getString("name"));
				coordinate.setCoordinateX(rs.getInt("coordinate_x"));
				coordinate.setCoordinateY(rs.getInt("coordinate_y"));

				coordinateList.add(coordinate);

			}
			
			return coordinateList;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new XyException(ExceptionMsg.SQL.getValue(), e);
		}

	}

}
