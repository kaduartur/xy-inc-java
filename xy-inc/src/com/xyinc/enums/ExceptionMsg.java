package com.xyinc.enums;

public enum ExceptionMsg {
	SQL("Ocorreu um erro inesperado com o banco de dados"), 
	POST("Ocorreu um erro ao tentar cadastrar"),
	GET("Ocorreu um erro ao realizar busca");
	
	private final String msg;
	
	ExceptionMsg(String msg){
		this.msg = msg;
	}
	
	public String getValue(){
		return msg;
	}
}
