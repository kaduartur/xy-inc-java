# README #

### What is this repository for? ###

* Teste para vaga de desenvolvedor back-end na Zup
* V1.0

### How do I get set up? ###

* git clone no repositorio;
* abra a pasta do repositorio e execute o arquivo "xy-inc-db.sql" no MySQL Workbench para criar o banco de dados;
* Abra o eclipse, clique em file -> import -> Maven -> Existing Maven Projects -> selecione a pasta do repositorio;
* Ap�s abrir o projeto altere o user/password do banco de dados na classe "ConnectionFactory.java" na aplica��o;
* Adicione um servidor Tomcat(v9.0) e o inicie;
* Utilize o postman para testar;
* Adicionar POI -> "http://localhost:8080/xy-inc/rest/coordinateRest/createCoordinate" - metodo: POST - body json -> { "name": "Teste", "coordinateX": 0, "coordinateY": 0 }
* Listar todos os POIs -> "http://localhost:8080/xy-inc/rest/coordinateRest/getAllCoordinates" - metodo: GET
* Listar os POIs por proximidade -> http://localhost:8080/xy-inc/rest/coordinateRest/getPointsInterestByProximity/20/10" - metodo: GET

### Who do I talk to? ###

* Kadu Artur Prussek
* kadu.artur@gmail.com | 047 98917-2809